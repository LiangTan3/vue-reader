import { mapGetters, mapActions } from 'vuex'
import { addCss, removeAllCss } from '../utils/book'
import { saveLocation, getReadTime, getBookmark } from '../utils/localStorage'

export const ebookMixin = {
  computed: {
    ...mapGetters([
      'fileName',
      'menuVisible',
      'settingVisible',
      'defaultFontSize',
      'currentBook',
      'defaultFontFamily',
      'fontFamilyVisible',
      'defaultTheme',
      'bookAvailable',
      'progress',
      'section',
      'isPaginating',
      'navigation',
      'cover',
      'metadata',
      'paginate',
      'pagelist',
      'offsetY',
      'isBookmark'
    ]),
    getReadTimeText: function () {
      const time = getReadTime(this.fileName)
      if (!time) {
        return 0
      }
      const minutes = Math.ceil(time / 60)
      return minutes
    },
    getSectionName: function () {
      return this.section ? this.navigation[this.section].label : ' '
    }
  },
  methods: {
    ...mapActions([
      'setMenuVisible',
      'setFileName',
      'setSettingVisible',
      'setDefaultFontSize',
      'setCurrentBook',
      'setDefaultFontFamily',
      'setFontFamilyVisible',
      'setDefaultTheme',
      'setBookAvailable',
      'setProgress',
      'setSection',
      'setIsPaginating',
      'setNavigation',
      'setCover',
      'setMetadata',
      'setPaginate',
      'setPagelist',
      'setOffsetY',
      'setIsBookmark'
    ]),
    initGlobalStyle () {
      removeAllCss()
      if (this.defaultTheme === 'Default') {
        addCss(`${process.env.VUE_APP_RES_URL}/theme/theme_default.css`)
      } else if (this.defaultTheme === 'Gold') {
        addCss(`${process.env.VUE_APP_RES_URL}/theme/theme_gold.css`)
      } else if (this.defaultTheme === 'Eye') {
        addCss(`${process.env.VUE_APP_RES_URL}/theme/theme_eye.css`)
      } else if (this.defaultTheme === 'Night') {
        addCss(`${process.env.VUE_APP_RES_URL}/theme/theme_night.css`)
      } else {
        addCss(`${process.env.VUE_APP_RES_URL}/theme/theme_default.css`)
      }
    },
    refreshLocation () {
      // get current location , and get the progress from the current location cfi object
      // progress is a numeric value , convert to percentage
      const currentLocation = this.currentBook.rendition.currentLocation()
      if (!currentLocation || !currentLocation.start) {
        return
      }
      const startCfi = currentLocation.start.cfi
      let progress = this.currentBook.locations.percentageFromCfi(startCfi)
      progress = Math.floor(progress * 100)
      this.setProgress(progress).then(() => {
        saveLocation(this.fileName, startCfi)
      })
      this.setSection(currentLocation.start.index)
      const bookmark = getBookmark(this.fileName)
      if (bookmark && bookmark.some(item => item.cfi === startCfi)) {
        this.setIsBookmark(true)
      } else {
        this.setIsBookmark(false)
      }
    },
    // target can be locaton ,...
    display (target, cb) {
      if (target) {
        this.currentBook.rendition.display(target).then(() => {
          this.refreshLocation()
          if (cb) cb()
        })
      } else {
        this.currentBook.rendition.display().then(() => {
          this.refreshLocation()
          if (cb) cb()
        })
      }
    },
    hideTitleAndMenu () {
      // this.$store.dispatch('setMenuVisible', false)
      this.setMenuVisible(false)
      this.setSettingVisible(-1)
      this.setFontFamilyVisible(false)
    }
  }
}
