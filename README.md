# vue-reader

An Customized Ebook Reader Built with Vue (Vue.js+ Vuex + Vue router) and Epub.js.


# Ebook Reader Tool 

## Project setup

All the theme styles and epub book resources should be access by the browser via nginx server. The nginx can be downloaded from the [offical website](http://nginx.org/en/download.html)


### Nginx Configuration

Here, I use nginx-1.18.0. Edit the nginx.conf file under the ```nginx-1.18.0\conf```  folder. 
Add a new server as follows:

```
server {
  listen       8081;
  server_name   resource;
  root   <the static resource folder>;
  autoindex  on;
  location / {
    add_header Access-Control-Allow-Origin *;
  }
  add_header Cache-Control "no-cache, must-revalidate"; 
}
```

After that, run following commands on terminal

```bash
nginx -t
start nginx
nginx -s reload # [Optional] restart the nginx if the configuration file has been changed
```

### Install dependencies

```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).




## Project Dependencies:

* epubjs: 0.3.71,
* vue: 2.6.11,
* vue-router: 3.2.0,
* vuex: 3.4.0
* web-storage-cache : 1.1.1,





### Features:

* Render epubc format ebook accoring to the url
* Font-size and Font-Fmaily Setting.
* Theme (skin) Setting.
* Searching the Entire Book 
* Book Chapter Navigation
* Add/Remove Bookmark
* A Simple Pagination Algorithm 



### Detail:

* Render epubc format ebook 

<div>
  <img src="./pic/basic_feature.gif" alt="figure 9" width="50%" height="50%">
</div>

* Font-size and Font-Fmaily Setting.

<div>
  <img src="./pic/font_size_family.gif" alt="figure 9" width="50%" height="50%">
</div>

* Theme (skin) Setting.

<div>
  <img src="./pic/background.gif" alt="figure 9" width="50%" height="50%">
</div>

* Searching the Entire Book 

<div>
  <img src="./pic/search.gif" alt="figure 9" width="50%" height="50%">
</div>

* Book Chapter Navigation

<div>
  <img src="./pic/navigation.gif" alt="figure 9" width="50%" height="50%">
</div>

* Add Bookmark

<div>
  <img src="./pic/save_bookmark.gif" alt="figure 9" width="50%" height="50%">
</div>

* Remove Bookmark

<div>
  <img src="./pic/delete_bookmark.gif" alt="figure 9" width="50%" height="50%">
</div>

  
* A Simple Pagination Algorithm 


```javascript
// pagination algorithm
  this.book.ready.then(() => {
    // 1. pagination has two factors: window Width & font size
    // paginating is to split the whole ebook dom to list of location cfi
    return this.book.locations.generate(750 * (window.innerWidth / 375) *
    getFontSize(this.fileName) / 16)
  }).then(locations => {
  // 2. init pageList for each section.
  this.navigation.forEach(nav => {
    nav.pageList = []
  })
  // 3. classify each location to one specific navigation(section)
  // if location cfi has the same section name as in nav.href
  locations.forEach(item => {
    const loc = item.match(/\[(.*)\]!/)[1]
    this.navigation.forEach(nav => {
      if (nav.href) {
        const href = nav.href.match(/^(.*)\.html$/)[1]
        if (href === loc) {
          nav.pageList.push(item)
        }
      }
    })
  })
  // 4. compute the start page number based on each section total pages
  let currentPage = 1
  this.navigation.forEach((nav, index) => {
    if (index === 0) {
      nav.page = 1
    } else {
      nav.page = currentPage
    }
    currentPage += nav.pageList.length + 1
  })
```



### Dev Tool :

* Chrome 
* Visual studio code
* Google Pixel 3



### TODO:

Add Ebook Management Pages











